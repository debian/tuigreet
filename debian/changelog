tuigreet (0.9.1-5) unstable; urgency=medium

  [ Peter Michael Green ]
  * Remove upper limit from Cargo dependencies on rust-ini, i18n-embed and
    i18n-embed-impl. (Closes: #1091453)
  * Add patch to support i18n-embed 0.15 with i18n-embed-fl 0.9 and bump
    dependencies accordgingly. (Closes: #1091560)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Mon, 13 Jan 2025 16:57:18 +0100

tuigreet (0.9.1-4) unstable; urgency=medium

  [ Peter Michael Green ]
  * Non-maintainer upload.
  * Remove upper limit from Cargo dependency on nix. (Closes: #1085461)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Sun, 24 Nov 2024 18:51:09 +0100

tuigreet (0.9.1-3) unstable; urgency=medium

  * Backport upstream commit to remove control characters added by fluent and
    rendered improperly by ratatui 0.27 (Closes: #1081392)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Wed, 11 Sep 2024 19:11:15 +0200

tuigreet (0.9.1-2) unstable; urgency=medium

  [ Matthias Klose ]
  * Update ratatui dependency. (Closes: #1081202)
  * Revert the uzers downgrade.

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Tue, 10 Sep 2024 19:34:23 +0200

tuigreet (0.9.1-1) unstable; urgency=medium

  [ Blair Noctis ]
  * New upstream release.
  * debian/patches/downgrade-uzers-0.11.patch

  [ Johannes Schauer Marin Rodrigues ]
  * mv debian/tuigreet.tmpfile -> debian/tuigreet.tmpfiles

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Thu, 29 Aug 2024 10:53:33 +0200

tuigreet (0.8.0-4) unstable; urgency=medium

  * debian/tuigreet.tmpfile: set up /var/cache/tuigreet owned by _greetd
    (Closes: #1074354)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Thu, 27 Jun 2024 17:41:07 +0200

tuigreet (0.8.0-3) unstable; urgency=medium

  * use nix crate feature "feature" to have nix::sys::utsname available
    (closes: #1071726)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Fri, 24 May 2024 14:42:43 +0200

tuigreet (0.8.0-2) unstable; urgency=medium

  * debian/rules: update env vars
  * debian/rules: immediately expand PATH to avoid recursive self reference
  * debian/control: add libstd-rust-dev to B-D to fix cross-compilation

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Sun, 21 Apr 2024 23:10:53 +0200

tuigreet (0.8.0-1) unstable; urgency=medium

  * Initial release. (Closes: #1067216)

 -- Johannes Schauer Marin Rodrigues <josch@debian.org>  Wed, 20 Mar 2024 15:38:26 +0100
